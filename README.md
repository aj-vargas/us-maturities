# us-maturities

See the yield curve of the constant maturities (nominal) released by the U.S. Federal Reserve.

A deployed instance of this app can be seen [here](https://www.aj-vargas.me/us-maturities).

Data obtained from the U.S. Federal Reserve [Data Download Program](https://www.federalreserve.gov/datadownload/Choose.aspx?rel=H15).
