/* global document io */

import Vue from 'vue';
import UsMaturities from './us-maturities.vue';

const TWEETS_LIMIT = 10;
const UsMaturitiesConstructor = Vue.extend(UsMaturities);

document.addEventListener('DOMContentLoaded', () => {
  const component = new UsMaturitiesConstructor({el: 'vue-app'});
  if (typeof io !== 'undefined') {
    const socket = io('https://io.aj-vargas.me/us-maturities-tweets');
    socket.on('snapshot', snapshot => {
      snapshot.sort(sortByTimeDesc);
      component.tweets.splice(0);
      component.tweets.unshift(...snapshot.splice(0, TWEETS_LIMIT));
    });
    socket.on('tweet', tweet => {
      component.tweets.unshift(tweet);
      component.tweets.splice(TWEETS_LIMIT);
    });
  }
});

function sortByTimeDesc(a, b) {
  if (a.timestamp < b.timestamp) {
    return 1;
  }
  if (a.timestamp > b.timestamp) {
    return -1;
  }
  return 0;
}
