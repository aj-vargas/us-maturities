import ky from 'ky';
import moment from 'moment-timezone/moment-timezone';
import {alert} from 'notie';
import Chartist from './chartist-with-1143-and-tpr.min';

const US_MATURITIES_API = 'https://api.aj-vargas.me/v0/us-maturities';

moment.tz.load(require('./filtered-tz-data.json'));

export default {
  name: 'UsMaturities',
  data() {
    return {
      first: moment.tz('1962-01-02', 'YYYY-MM-DD', 'America/New_York'),
      last: moment.tz('America/New_York'),
      isBlocked: true,
      selection: moment.tz('America/New_York'),
      maturitiesSeries: {},
      labels: [
        'm1', 'm3', 'm6', 'y1', 'y2', 'y3', 'y5', 'y7', 'y10', 'y20', 'y30'
      ],
      chart: null,
      tweets: []
    };
  },
  async mounted() {
    this.chart = new Chartist.Line('.us-maturities__chart', {
      labels: this.labels,
      series: [[]]
    }, {
      showArea: true,
      showPoint: false,
      axisY: {showGrid: false, showLabel: false}
    });
    await this.update();
  },
  computed: {
    latest() {
      return this.last.format('YYYY-MM-DD');
    },
    year() {
      return this.selection.format('YYYY');
    },
    month() {
      return this.selection.format('MMM').toUpperCase();
    },
    day() {
      return this.selection.format('DD');
    }
  },
  methods: {
    async addYear(value) {
      if (this.isBlocked) {
        return;
      }
      const wanted = this.selection.clone().add(value, 'year');
      if (wanted.isBefore(this.first, 'day') ||
          wanted.isAfter(this.last, 'day')
      ) {
        return;
      }
      await this.update(wanted.format('YYYY-MM-DD'));
    },
    async addMonth(value) {
      if (this.isBlocked) {
        return;
      }
      const wanted = this.selection.clone().add(value, 'month');
      if (wanted.isBefore(this.first, 'day') ||
          wanted.isAfter(this.last, 'day')
      ) {
        return;
      }
      await this.update(wanted.format('YYYY-MM-DD'));
    },
    async addDay(value) {
      if (this.isBlocked) {
        return;
      }
      const wanted = this.selection.clone().add(value, 'day');
      if (wanted.isBefore(this.first, 'day') ||
          wanted.isAfter(this.last, 'day')
      ) {
        return;
      }
      await this.update(wanted.format('YYYY-MM-DD'));
    },
    async to(timestamp) {
      if (this.isBlocked) {
        return;
      }
      await this.update(timestamp || this.last.format('YYYY-MM-DD'));
    },
    async update(timestamp = '') {
      try {
        this.isBlocked = true;
        if (timestamp && this.maturitiesSeries[timestamp]) {
          this.selection = moment.tz(
            timestamp,
            'YYYY-MM-DD',
            'America/New_York'
          );
          this.chart.data.series[0] = this.maturitiesSeries[timestamp];
          const showLabel = askShowLabel(this.chart.data.series[0]);
          this.chart.update(null, {axisY: {showLabel}}, true);
          !showLabel && alert({type: 'info', text: 'Day with no datum.'});
          this.isBlocked = false;
          return;
        }
        const response = await ky.get(
          timestamp,
          {prefixUrl: US_MATURITIES_API}
        ).json();
        if (response.errorCode) {
          alert({type: 'error', text: response.userMessage});
          this.isBlocked = false;
          return;
        }
        if (response.data.usMaturities.length) {
          buildMaturitiesSeries(
            this.maturitiesSeries,
            this.labels,
            response.data.usMaturities
          );
          this.selection = moment.tz(
            response.data.usMaturities[0].timestamp,
            'YYYY-MM-DD',
            'America/New_York'
          );
          if (!timestamp) {
            this.last = this.selection.clone();
          }
        } else if (timestamp) {
          this.maturitiesSeries[timestamp] = [];
          this.selection = moment.tz(
            timestamp,
            'YYYY-MM-DD',
            'America/New_York'
          );
        }
        this.chart.data.series[0] = this.maturitiesSeries[
          this.selection.format('YYYY-MM-DD')
        ];
        const showLabel = askShowLabel(this.chart.data.series[0]);
        this.chart.update(null, {axisY: {showLabel}}, true);
        !showLabel && alert({type: 'info', text: 'Day with no datum.'});
      } catch {
        alert({type: 'error', text: 'There was an error. Try again.'});
      }
      this.isBlocked = false;
    }
  }
};

function buildMaturitiesSeries(target, labels, maturities) {
  for (const label of labels) {
    for (const maturity of maturities) {
      if (!target[maturity.timestamp]) {
        target[maturity.timestamp] = [];
      }
      target[maturity.timestamp].push(maturity[label]);
    }
  }
}

function askShowLabel(series) {
  if (!series.length) {
    return false;
  }
  for (const serie of series) {
    if (serie) {
      return true;
    }
  }
  return false;
}
