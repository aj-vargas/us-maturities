#!/usr/bin/env bash

npx --no-install webpack --display-modules --sort-modules-by=size --mode=$1 --config webpack.config.js
